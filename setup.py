#!/usr/bin/env python

from xml.etree import ElementTree

# Global Vars
SUPPORTED_ELEMENTS = [ 'carbon' ]

def setparams(structfile):
    base_struct = ElementTree.parse(structfile).getroot()
    STRUCT_PROPS = {
            "monomer"   : base_struct.find('atom').items()[0][1],
            "hybrid"    : base_struct.find('atom').items()[1][1],
            "filled"    : base_struct.find('atom').items()[2][1],
            "bdim"      : base_struct.find('atom').items()[3][1]
            }

    return STRUCT_PROPS


