#!/usr/bin/env python

from sys import argv

# Utility functions
def getopt(arg):
    return argv[ argv.index(arg) + 1 ]

# Utility classes
class Atom(object):

    """Base Class describing an Atom"""

    def __init__(self, element='carbon'):
        self.next    = Atom 
        self.perv    = Atom
        self.kinetic = 0 # eV

if __name__ == '__main__':
    A1 = Atom()
    print(A1.next().kinetic)
