#!/usr/bin/env python

import numpy as np 
import matplotlib.pyplot as plt
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D as ax
from sys import argv

class Sim(object):
    def __init__(self, _x, _y, _tension, input_eV = 10):
        self.tension = _tension 
        self.grid = np.zeros((_x, _y))
        self.grid[_y//2][_x//2] = input_eV
        self.coordinates = [(x,y) for x in range(_x) \
                                  for y in range(_y) ]
    
    def propagate(self, step_no):
        # TODO: Program edge cases
        # I just realized they are literal edges :P
        stepped_cells = []
        for (x, y) in self.coordinates:
            if self.grid[y][x] > 0 and (x, y) not in stepped_cells:
                try:
                    spread = (self.grid[y][x] / 4) * self.tension 

                    self.grid[y + 1][x] += spread
                    self.grid[y - 1][x] += spread
                    self.grid[y][x + 1] += spread
                    self.grid[y][x - 1] += spread
                    
                    self.grid[y][x] -= spread * 4

                    stepped_cells += [ (x, y), (x+1, y), (x-1, y), (x, y+1), (x, y-1) ]
                except IndexError:
                    print("Boundry error. Passing!")
                    pass
                    
        
if __name__ == '__main__':

    # possible_params = [ ['--outfile', '--steps', '-X', '-Y'],
                        # ['-o', '-s', '-X', '-Y'] ]

    # if set(argv).issuperset(possible_params[0]):
        # outfile = argv[argv.index(possible_params[0][0])+1]

    # else:
    outfile = "outfile.png"
    x, y    = 100, 100
    tension = 1./2. 

    Simulation = Sim(x, y, tension, input_eV = 100)
    xx, yy  = np.meshgrid(range(x), range(y))

    fig = plt.figure()
    ax = fig.gca(projection='3d')

    for i in range(15):
        Simulation.propagate(i)
        ax.cla()
        ax.set_zlim(0, 100)
        ax.plot_surface(xx, yy, Simulation.grid, cmap=cm.coolwarm)
        plt.savefig('outfile{}'.format(i), dpi=600)
    
    ax.set_zlim(0, 100)
    ax.cla()
    ax.plot_surface(xx, yy, Simulation.grid, cmap=cm.coolwarm)
    plt.savefig(outfile)
    plt.show()
