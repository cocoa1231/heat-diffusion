#!/usr/bin/env python

import setup
from utils import *


class Network(object):
    def __init__(self):
        SFILE  = getopt('--sfile')
        self.PARAMS = setup.setparams(SFILE)
        
        if not self.PARAMS['monomer'] in setup.SUPPORTED_ELEMENTS:
            raise NotImplementedError('{} is not supported yet! Please modify structure.xml'.format(self.PARAMS['monomer']))
        
            

if __name__ == "__main__":
    Solid = Network()
